cd %~dp0
docker rm -f intro_to_containers_database_1
docker rm -f intro_to_containers_staytus_1

docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.with-stored-database.yml down --remove-orphans

docker-compose -f docker-compose.with-stored-database.yml up -d --build --remove-orphans
REM Press a key to continue
pause
docker exec -it intro_to_containers-1 /bin/bash